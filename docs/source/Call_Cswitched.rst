Cswitched
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:EVDO:SIGNaling<Instance>:CSWitched:ACTion

.. code-block:: python

	CALL:EVDO:SIGNaling<Instance>:CSWitched:ACTion



.. autoclass:: RsCmwEvdoSig.Implementations.Call_.Cswitched.Cswitched
	:members:
	:undoc-members:
	:noindex: