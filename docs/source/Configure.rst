Configure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:DISPlay
	single: CONFigure:EVDO:SIGNaling<Instance>:ETOE

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:DISPlay
	CONFigure:EVDO:SIGNaling<Instance>:ETOE



.. autoclass:: RsCmwEvdoSig.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Test.rst
	Configure_RfSettings.rst
	Configure_Fading.rst
	Configure_IqIn.rst
	Configure_Carrier.rst
	Configure_Sector.rst
	Configure_Pilot.rst
	Configure_Cstatus.rst
	Configure_Mac.rst
	Configure_Layer.rst
	Configure_Network.rst
	Configure_Handoff.rst
	Configure_Mmonitor.rst
	Configure_Application.rst
	Configure_RfPower.rst
	Configure_RpControl.rst
	Configure_System.rst
	Configure_Ncell.rst
	Configure_Connection.rst
	Configure_RxQuality.rst