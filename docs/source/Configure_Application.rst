Application
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:APPLication:DSIGnaling
	single: CONFigure:EVDO:SIGNaling<Instance>:APPLication:MODE
	single: CONFigure:EVDO:SIGNaling<Instance>:APPLication

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:APPLication:DSIGnaling
	CONFigure:EVDO:SIGNaling<Instance>:APPLication:MODE
	CONFigure:EVDO:SIGNaling<Instance>:APPLication



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Application.Application
	:members:
	:undoc-members:
	:noindex: