Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:CARRier:SETTing
	single: CONFigure:EVDO:SIGNaling<Instance>:CARRier:CHANnel
	single: CONFigure:EVDO:SIGNaling<Instance>:CARRier:FLFRequency
	single: CONFigure:EVDO:SIGNaling<Instance>:CARRier:RLFRequency

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:CARRier:SETTing
	CONFigure:EVDO:SIGNaling<Instance>:CARRier:CHANnel
	CONFigure:EVDO:SIGNaling<Instance>:CARRier:FLFRequency
	CONFigure:EVDO:SIGNaling<Instance>:CARRier:RLFRequency



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.carrier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Carrier_Level.rst