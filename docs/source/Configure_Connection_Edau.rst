Edau
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:CONNection:EDAU:ENABle
	single: CONFigure:EVDO:SIGNaling<Instance>:CONNection:EDAU:NSEGment
	single: CONFigure:EVDO:SIGNaling<Instance>:CONNection:EDAU:NID

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:CONNection:EDAU:ENABle
	CONFigure:EVDO:SIGNaling<Instance>:CONNection:EDAU:NSEGment
	CONFigure:EVDO:SIGNaling<Instance>:CONNection:EDAU:NID



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Connection_.Edau.Edau
	:members:
	:undoc-members:
	:noindex: