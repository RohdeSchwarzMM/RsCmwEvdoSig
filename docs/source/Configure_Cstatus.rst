Cstatus
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:AFLCarriers
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:ARLCarriers
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:PLSubtype
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:IRAT
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:APPLication
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:UATI
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:ESN
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:MEID
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:EHRPd
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:LOG
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:ILCMask
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:QLCMask
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:MRBandwidth
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:MODE

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:AFLCarriers
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:ARLCarriers
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:PLSubtype
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:IRAT
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:APPLication
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:UATI
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:ESN
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:MEID
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:EHRPd
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:LOG
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:ILCMask
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:QLCMask
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:MRBandwidth
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:MODE



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Cstatus.Cstatus
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cstatus.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cstatus_PcChannel.rst