PcChannel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:PCCHannel:ENABle
	single: CONFigure:EVDO:SIGNaling<Instance>:CSTatus:PCCHannel:CYCLe

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:PCCHannel:ENABle
	CONFigure:EVDO:SIGNaling<Instance>:CSTatus:PCCHannel:CYCLe



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Cstatus_.PcChannel.PcChannel
	:members:
	:undoc-members:
	:noindex: