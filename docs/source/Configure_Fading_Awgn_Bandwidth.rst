Bandwidth
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:FADing:AWGN:BWIDth:RATio
	single: CONFigure:EVDO:SIGNaling<Instance>:FADing:AWGN:BWIDth:NOISe

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:FADing:AWGN:BWIDth:RATio
	CONFigure:EVDO:SIGNaling<Instance>:FADing:AWGN:BWIDth:NOISe



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Fading_.Awgn_.Bandwidth.Bandwidth
	:members:
	:undoc-members:
	:noindex: