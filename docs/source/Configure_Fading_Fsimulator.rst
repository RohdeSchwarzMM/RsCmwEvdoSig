Fsimulator
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:KCONstant
	single: CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:ENABle
	single: CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:STANdard

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:KCONstant
	CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:ENABle
	CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:STANdard



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Fading_.Fsimulator.Fsimulator
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.fsimulator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Fsimulator_Globale.rst
	Configure_Fading_Fsimulator_Restart.rst
	Configure_Fading_Fsimulator_Iloss.rst