Iloss
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:MODE
	single: CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:LOSS
	single: CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:CSAMples

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:MODE
	CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:LOSS
	CONFigure:EVDO:SIGNaling<Instance>:FADing:FSIMulator:ILOSs:CSAMples



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Fading_.Fsimulator_.Iloss.Iloss
	:members:
	:undoc-members:
	:noindex: