Handoff
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:HANDoff:BCLass
	single: CONFigure:EVDO:SIGNaling<Instance>:HANDoff:CHANnel

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:HANDoff:BCLass
	CONFigure:EVDO:SIGNaling<Instance>:HANDoff:CHANnel



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Handoff.Handoff
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.handoff.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Handoff_Carrier.rst
	Configure_Handoff_Network.rst