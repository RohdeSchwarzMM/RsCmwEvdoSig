Carrier
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:HANDoff:CARRier:CHANnel
	single: CONFigure:EVDO:SIGNaling<Instance>:HANDoff:CARRier:FLFRequency
	single: CONFigure:EVDO:SIGNaling<Instance>:HANDoff:CARRier:RLFRequency

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:HANDoff:CARRier:CHANnel
	CONFigure:EVDO:SIGNaling<Instance>:HANDoff:CARRier:FLFRequency
	CONFigure:EVDO:SIGNaling<Instance>:HANDoff:CARRier:RLFRequency



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Handoff_.Carrier.Carrier
	:members:
	:undoc-members:
	:noindex: