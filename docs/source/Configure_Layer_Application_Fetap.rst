Fetap
----------------------------------------





.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Fetap.Fetap
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.layer.application.fetap.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Layer_Application_Fetap_Drc.rst
	Configure_Layer_Application_Fetap_Lback.rst
	Configure_Layer_Application_Fetap_Ack.rst