Drc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:TYPE
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:INDex
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:SIZE
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:RATE
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:SLOTs

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:TYPE
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:INDex
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:SIZE
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:RATE
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FETap:DRC:SLOTs



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Fetap_.Drc.Drc
	:members:
	:undoc-members:
	:noindex: