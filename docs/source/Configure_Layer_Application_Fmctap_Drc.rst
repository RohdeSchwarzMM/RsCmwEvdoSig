Drc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:TYPE
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:INDex
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:SIZE
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:RATE
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:SLOTs

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:TYPE
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:INDex
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:SIZE
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:RATE
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FMCTap:DRC:SLOTs



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Fmctap_.Drc.Drc
	:members:
	:undoc-members:
	:noindex: