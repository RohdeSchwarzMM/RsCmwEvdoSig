Ftap
----------------------------------------





.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Ftap.Ftap
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.layer.application.ftap.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Layer_Application_Ftap_Drc.rst
	Configure_Layer_Application_Ftap_Lback.rst
	Configure_Layer_Application_Ftap_Ack.rst