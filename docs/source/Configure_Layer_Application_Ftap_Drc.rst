Drc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FTAP:DRC:INDex
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FTAP:DRC:RATE
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FTAP:DRC:SLOTs

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FTAP:DRC:INDex
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FTAP:DRC:RATE
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:FTAP:DRC:SLOTs



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Ftap_.Drc.Drc
	:members:
	:undoc-members:
	:noindex: