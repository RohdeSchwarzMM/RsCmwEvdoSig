Packet
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:PACKet:PREFerred
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:PACKet:MODE

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:PACKet:PREFerred
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:PACKet:MODE



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Packet.Packet
	:members:
	:undoc-members:
	:noindex: