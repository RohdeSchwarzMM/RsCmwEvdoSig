Smin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RETap:SMIN:INDex
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RETap:SMIN:SIZE

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RETap:SMIN:INDex
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RETap:SMIN:SIZE



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Retap_.Smin.Smin
	:members:
	:undoc-members:
	:noindex: