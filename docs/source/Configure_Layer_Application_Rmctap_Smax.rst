Smax
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RMCTap:SMAX:INDex
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RMCTap:SMAX:SIZE

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RMCTap:SMAX:INDex
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RMCTap:SMAX:SIZE



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Rmctap_.Smax.Smax
	:members:
	:undoc-members:
	:noindex: