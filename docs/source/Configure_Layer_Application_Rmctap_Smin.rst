Smin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RMCTap:SMIN:INDex
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RMCTap:SMIN:SIZE

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RMCTap:SMIN:INDex
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RMCTap:SMIN:SIZE



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Rmctap_.Smin.Smin
	:members:
	:undoc-members:
	:noindex: