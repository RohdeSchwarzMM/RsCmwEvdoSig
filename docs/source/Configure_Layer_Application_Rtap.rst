Rtap
----------------------------------------





.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Rtap.Rtap
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.layer.application.rtap.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Layer_Application_Rtap_Rmin.rst
	Configure_Layer_Application_Rtap_Rmax.rst