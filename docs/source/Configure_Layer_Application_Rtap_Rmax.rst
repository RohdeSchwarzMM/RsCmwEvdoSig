Rmax
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RTAP:RMAX:INDex
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RTAP:RMAX:RATE

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RTAP:RMAX:INDex
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RTAP:RMAX:RATE



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Rtap_.Rmax.Rmax
	:members:
	:undoc-members:
	:noindex: