Rmin
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RTAP:RMIN:INDex
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RTAP:RMIN:RATE

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RTAP:RMIN:INDex
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:APPLication:RTAP:RMIN:RATE



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Application_.Rtap_.Rmin.Rmin
	:members:
	:undoc-members:
	:noindex: