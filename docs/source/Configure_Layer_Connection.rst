Connection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:CONNection:ROMessages
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:CONNection:PDTHreshold
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:CONNection:RLFoffset

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:CONNection:ROMessages
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:CONNection:PDTHreshold
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:CONNection:RLFoffset



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Connection.Connection
	:members:
	:undoc-members:
	:noindex: