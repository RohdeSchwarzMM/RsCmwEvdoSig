DftProtocol
----------------------------------------





.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Mac_.DftProtocol.DftProtocol
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.layer.mac.dftProtocol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Layer_Mac_DftProtocol_Drc.rst
	Configure_Layer_Mac_DftProtocol_Ack.rst