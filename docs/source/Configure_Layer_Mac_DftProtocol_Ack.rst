Ack
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DFTProtocol:ACK:CGAin

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DFTProtocol:ACK:CGAin



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Mac_.DftProtocol_.Ack.Ack
	:members:
	:undoc-members:
	:noindex: