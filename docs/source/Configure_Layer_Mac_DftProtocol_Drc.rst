Drc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DFTProtocol:DRC:COVer
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DFTProtocol:DRC:LENGth
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DFTProtocol:DRC:CGAin

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DFTProtocol:DRC:COVer
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DFTProtocol:DRC:LENGth
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DFTProtocol:DRC:CGAin



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Mac_.DftProtocol_.Drc.Drc
	:members:
	:undoc-members:
	:noindex: