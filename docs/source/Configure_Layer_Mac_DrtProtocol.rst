DrtProtocol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DRTProtocol:DONom
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DRTProtocol:DRATe
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DRTProtocol:ITRansition
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DRTProtocol:DTRansition

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DRTProtocol:DONom
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DRTProtocol:DRATe
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DRTProtocol:ITRansition
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:DRTProtocol:DTRansition



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Mac_.DrtProtocol.DrtProtocol
	:members:
	:undoc-members:
	:noindex: