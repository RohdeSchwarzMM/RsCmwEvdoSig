EftProtocol
----------------------------------------





.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Mac_.EftProtocol.EftProtocol
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.layer.mac.eftProtocol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Layer_Mac_EftProtocol_Drc.rst
	Configure_Layer_Mac_EftProtocol_Dsc.rst
	Configure_Layer_Mac_EftProtocol_Ack.rst