Ack
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:ACK:CGAin

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:ACK:CGAin



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Mac_.EftProtocol_.Ack.Ack
	:members:
	:undoc-members:
	:noindex: