Drc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DRC:COVer
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DRC:LENGth
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DRC:CGAin

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DRC:COVer
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DRC:LENGth
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DRC:CGAin



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Mac_.EftProtocol_.Drc.Drc
	:members:
	:undoc-members:
	:noindex: