Dsc
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DSC:VALue
	single: CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DSC:CGAin

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DSC:VALue
	CONFigure:EVDO:SIGNaling<Instance>:LAYer:MAC:EFTProtocol:DSC:CGAin



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Layer_.Mac_.EftProtocol_.Dsc.Dsc
	:members:
	:undoc-members:
	:noindex: