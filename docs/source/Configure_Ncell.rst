Ncell
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:RLMeutra
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:THRServing
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:MRTimer

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NCELl:RLMeutra
	CONFigure:EVDO:SIGNaling<Instance>:NCELl:THRServing
	CONFigure:EVDO:SIGNaling<Instance>:NCELl:MRTimer



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Ncell.Ncell
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ncell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ncell_All.rst
	Configure_Ncell_Evdo.rst
	Configure_Ncell_Cdma.rst
	Configure_Ncell_Lte.rst