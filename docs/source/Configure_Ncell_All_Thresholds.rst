Thresholds
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:ALL:THResholds:LOW
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:ALL:THResholds

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NCELl:ALL:THResholds:LOW
	CONFigure:EVDO:SIGNaling<Instance>:NCELl:ALL:THResholds



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Ncell_.All_.Thresholds.Thresholds
	:members:
	:undoc-members:
	:noindex: