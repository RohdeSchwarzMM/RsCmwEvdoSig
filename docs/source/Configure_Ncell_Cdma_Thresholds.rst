Thresholds
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:CDMA:THResholds:LOW
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:CDMA:THResholds

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NCELl:CDMA:THResholds:LOW
	CONFigure:EVDO:SIGNaling<Instance>:NCELl:CDMA:THResholds



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Ncell_.Cdma_.Thresholds.Thresholds
	:members:
	:undoc-members:
	:noindex: