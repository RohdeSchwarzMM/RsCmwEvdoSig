Evdo
----------------------------------------





.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Ncell_.Evdo.Evdo
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ncell.evdo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ncell_Evdo_Cell.rst
	Configure_Ncell_Evdo_Thresholds.rst