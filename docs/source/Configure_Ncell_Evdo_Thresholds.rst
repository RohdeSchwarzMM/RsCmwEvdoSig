Thresholds
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:EVDO:THResholds:LOW
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:EVDO:THResholds

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NCELl:EVDO:THResholds:LOW
	CONFigure:EVDO:SIGNaling<Instance>:NCELl:EVDO:THResholds



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Ncell_.Evdo_.Thresholds.Thresholds
	:members:
	:undoc-members:
	:noindex: