Thresholds
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:LTE:THResholds:LOW
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:LTE:THResholds

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NCELl:LTE:THResholds:LOW
	CONFigure:EVDO:SIGNaling<Instance>:NCELl:LTE:THResholds



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Ncell_.Lte_.Thresholds.Thresholds
	:members:
	:undoc-members:
	:noindex: