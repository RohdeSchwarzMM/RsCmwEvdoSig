Thrx<NeighborCell>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.configure.ncell.lte.thrx.repcap_neighborCell_get()
	driver.configure.ncell.lte.thrx.repcap_neighborCell_set(repcap.NeighborCell.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NCELl:LTE:THRX<NeighborCell>

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NCELl:LTE:THRX<NeighborCell>



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Ncell_.Lte_.Thrx.Thrx
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ncell.lte.thrx.clone()