Network
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SID
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:RELease

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SID
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:RELease



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Network.Network
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.network.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Network_Sector.rst
	Configure_Network_Pilot.rst
	Configure_Network_PropertyPy.rst
	Configure_Network_Aprobes.rst
	Configure_Network_Security.rst