Aprobes
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:MODE
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:IADJust
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:OLADjust
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:PINCrement
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:PPSequence
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:PLENgth
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:ACDuration
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:PLSLots
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:SAMRate

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:MODE
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:IADJust
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:OLADjust
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:PINCrement
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:PPSequence
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:PLENgth
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:ACDuration
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:PLSLots
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:APRobes:SAMRate



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Network_.Aprobes.Aprobes
	:members:
	:undoc-members:
	:noindex: