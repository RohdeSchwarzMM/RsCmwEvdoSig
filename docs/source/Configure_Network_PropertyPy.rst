PropertyPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:PROPerty:CLDTime
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:PROPerty:FPACtivity
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:PROPerty:IRAT

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NETWork:PROPerty:CLDTime
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:PROPerty:FPACtivity
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:PROPerty:IRAT



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Network_.PropertyPy.PropertyPy
	:members:
	:undoc-members:
	:noindex: