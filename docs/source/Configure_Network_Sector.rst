Sector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:PNOFfset
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:CLRCode
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:SMASk
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:CNTCode
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:FORMat
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:NPBits
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:IDOVerall

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:PNOFfset
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:CLRCode
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:SMASk
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:CNTCode
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:FORMat
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:NPBits
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:IDOVerall



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Network_.Sector.Sector
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.network.sector.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Network_Sector_Id.rst