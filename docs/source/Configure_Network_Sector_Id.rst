Id
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:ID:ANSI
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:ID:MANual

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:ID:ANSI
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECTor:ID:MANual



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Network_.Sector_.Id.Id
	:members:
	:undoc-members:
	:noindex: