Security
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECurity:SKEY
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECurity:OPC
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECurity:AUTHenticat
	single: CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECurity:SQN

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECurity:SKEY
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECurity:OPC
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECurity:AUTHenticat
	CONFigure:EVDO:SIGNaling<Instance>:NETWork:SECurity:SQN



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Network_.Security.Security
	:members:
	:undoc-members:
	:noindex: