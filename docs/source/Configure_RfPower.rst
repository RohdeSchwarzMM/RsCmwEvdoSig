RfPower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RFPower:EVDO
	single: CONFigure:EVDO:SIGNaling<Instance>:RFPower:OUTPut
	single: CONFigure:EVDO:SIGNaling<Instance>:RFPower:EPMode
	single: CONFigure:EVDO:SIGNaling<Instance>:RFPower:MANual
	single: CONFigure:EVDO:SIGNaling<Instance>:RFPower:EXPected

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RFPower:EVDO
	CONFigure:EVDO:SIGNaling<Instance>:RFPower:OUTPut
	CONFigure:EVDO:SIGNaling<Instance>:RFPower:EPMode
	CONFigure:EVDO:SIGNaling<Instance>:RFPower:MANual
	CONFigure:EVDO:SIGNaling<Instance>:RFPower:EXPected



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RfPower.RfPower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfPower_Mode.rst
	Configure_RfPower_Level.rst