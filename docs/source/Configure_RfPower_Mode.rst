Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RFPower:MODE:AWGN

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RFPower:MODE:AWGN



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RfPower_.Mode.Mode
	:members:
	:undoc-members:
	:noindex: