RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RFSettings:EATTenuation
	single: CONFigure:EVDO:SIGNaling<Instance>:RFSettings:BCLass
	single: CONFigure:EVDO:SIGNaling<Instance>:RFSettings:FREQuency
	single: CONFigure:EVDO:SIGNaling<Instance>:RFSettings:FLFRequency
	single: CONFigure:EVDO:SIGNaling<Instance>:RFSettings:RLFRequency
	single: CONFigure:EVDO:SIGNaling<Instance>:RFSettings:FOFFset
	single: CONFigure:EVDO:SIGNaling<Instance>:RFSettings:CHANnel

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RFSettings:EATTenuation
	CONFigure:EVDO:SIGNaling<Instance>:RFSettings:BCLass
	CONFigure:EVDO:SIGNaling<Instance>:RFSettings:FREQuency
	CONFigure:EVDO:SIGNaling<Instance>:RFSettings:FLFRequency
	CONFigure:EVDO:SIGNaling<Instance>:RFSettings:RLFRequency
	CONFigure:EVDO:SIGNaling<Instance>:RFSettings:FOFFset
	CONFigure:EVDO:SIGNaling<Instance>:RFSettings:CHANnel



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex: