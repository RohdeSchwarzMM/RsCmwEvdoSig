RpControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RPControl:PCBits
	single: CONFigure:EVDO:SIGNaling<Instance>:RPControl:SSIZe
	single: CONFigure:EVDO:SIGNaling<Instance>:RPControl:REPetition
	single: CONFigure:EVDO:SIGNaling<Instance>:RPControl:RUN

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RPControl:PCBits
	CONFigure:EVDO:SIGNaling<Instance>:RPControl:SSIZe
	CONFigure:EVDO:SIGNaling<Instance>:RPControl:REPetition
	CONFigure:EVDO:SIGNaling<Instance>:RPControl:RUN



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RpControl.RpControl
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rpControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RpControl_Segment.rst