Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S4
	rc = driver.configure.rpControl.segment.repcap_segment_get()
	driver.configure.rpControl.segment.repcap_segment_set(repcap.Segment.S1)





.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RpControl_.Segment.Segment
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rpControl.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RpControl_Segment_Bits.rst
	Configure_RpControl_Segment_Length.rst