Bits
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RPControl:SEGMent<Segment>:BITS

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RPControl:SEGMent<Segment>:BITS



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RpControl_.Segment_.Bits.Bits
	:members:
	:undoc-members:
	:noindex: