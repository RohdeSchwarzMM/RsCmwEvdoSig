Length
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RPControl:SEGMent<Segment>:LENGth

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RPControl:SEGMent<Segment>:LENGth



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RpControl_.Segment_.Length.Length
	:members:
	:undoc-members:
	:noindex: