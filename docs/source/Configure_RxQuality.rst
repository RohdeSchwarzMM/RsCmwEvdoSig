RxQuality
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:UPERiod
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:REPetition

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:UPERiod
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:REPetition



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality.RxQuality
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Carrier.rst
	Configure_RxQuality_Rstatistics.rst
	Configure_RxQuality_Per.rst
	Configure_RxQuality_FlPer.rst
	Configure_RxQuality_RlPer.rst
	Configure_RxQuality_Throughput.rst
	Configure_RxQuality_FlPerformance.rst
	Configure_RxQuality_RlPerformance.rst
	Configure_RxQuality_Result.rst
	Configure_RxQuality_Limit.rst