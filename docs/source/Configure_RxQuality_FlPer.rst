FlPer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:FLPer:MTPSent
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:FLPer:SCONdition

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:FLPer:MTPSent
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:FLPer:SCONdition



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.FlPer.FlPer
	:members:
	:undoc-members:
	:noindex: