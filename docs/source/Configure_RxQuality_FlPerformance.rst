FlPerformance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance:MFRames

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance:MFRames



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.FlPerformance.FlPerformance
	:members:
	:undoc-members:
	:noindex: