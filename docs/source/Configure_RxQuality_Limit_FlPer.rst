FlPer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:FLPer:MPER
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:FLPer:CLEVel

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:FLPer:MPER
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:FLPer:CLEVel



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.Limit_.FlPer.FlPer
	:members:
	:undoc-members:
	:noindex: