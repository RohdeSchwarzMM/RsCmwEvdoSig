Per
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:PER:EVALuation

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:PER:EVALuation



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.Limit_.Per.Per
	:members:
	:undoc-members:
	:noindex: