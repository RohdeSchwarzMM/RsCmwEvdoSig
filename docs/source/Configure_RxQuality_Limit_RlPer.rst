RlPer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:RLPer:MPER
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:RLPer:CLEVel

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:RLPer:MPER
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:LIMit:RLPer:CLEVel



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.Limit_.RlPer.RlPer
	:members:
	:undoc-members:
	:noindex: