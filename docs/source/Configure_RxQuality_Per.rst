Per
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:PER:TOUT
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:PER:REPetition

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:PER:TOUT
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:PER:REPetition



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.Per.Per
	:members:
	:undoc-members:
	:noindex: