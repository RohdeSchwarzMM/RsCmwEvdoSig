Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:FLPer
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:RLPer
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:FLPFormance
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:RLPFormance
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:IPSTatistics

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:FLPer
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:RLPer
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:FLPFormance
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:RLPFormance
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RESult:IPSTatistics



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.Result.Result
	:members:
	:undoc-members:
	:noindex: