RlPer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RLPer:MPSent
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RLPer:SCONdition

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RLPer:MPSent
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RLPer:SCONdition



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.RlPer.RlPer
	:members:
	:undoc-members:
	:noindex: