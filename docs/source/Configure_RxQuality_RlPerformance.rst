RlPerformance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance:MFRames

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance:MFRames



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.RlPerformance.RlPerformance
	:members:
	:undoc-members:
	:noindex: