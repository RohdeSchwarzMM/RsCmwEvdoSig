Throughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:THRoughput:TOUT
	single: CONFigure:EVDO:SIGNaling<Instance>:RXQuality:THRoughput:REPetition

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:THRoughput:TOUT
	CONFigure:EVDO:SIGNaling<Instance>:RXQuality:THRoughput:REPetition



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.RxQuality_.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex: