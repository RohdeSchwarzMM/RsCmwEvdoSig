Sector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:SECTor:SETTing

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:SECTor:SETTing



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Sector.Sector
	:members:
	:undoc-members:
	:noindex: