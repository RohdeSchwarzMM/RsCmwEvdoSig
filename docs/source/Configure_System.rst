System
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:SYSTem:TSOurce
	single: CONFigure:EVDO:SIGNaling<Instance>:SYSTem:DATE
	single: CONFigure:EVDO:SIGNaling<Instance>:SYSTem:TIME
	single: CONFigure:EVDO:SIGNaling<Instance>:SYSTem:SYNC
	single: CONFigure:EVDO:SIGNaling<Instance>:SYSTem:ATIMe
	single: CONFigure:EVDO:SIGNaling<Instance>:SYSTem:LSEConds

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:SYSTem:TSOurce
	CONFigure:EVDO:SIGNaling<Instance>:SYSTem:DATE
	CONFigure:EVDO:SIGNaling<Instance>:SYSTem:TIME
	CONFigure:EVDO:SIGNaling<Instance>:SYSTem:SYNC
	CONFigure:EVDO:SIGNaling<Instance>:SYSTem:ATIMe
	CONFigure:EVDO:SIGNaling<Instance>:SYSTem:LSEConds



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.System.System
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.system.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_System_LtOffset.rst