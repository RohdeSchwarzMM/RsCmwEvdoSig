Cstatus
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:EVDO:SIGNaling<Instance>:TEST:CSTatus:MEID
	single: CONFigure:EVDO:SIGNaling<Instance>:TEST:CSTatus:ESN

.. code-block:: python

	CONFigure:EVDO:SIGNaling<Instance>:TEST:CSTatus:MEID
	CONFigure:EVDO:SIGNaling<Instance>:TEST:CSTatus:ESN



.. autoclass:: RsCmwEvdoSig.Implementations.Configure_.Test_.Cstatus.Cstatus
	:members:
	:undoc-members:
	:noindex: