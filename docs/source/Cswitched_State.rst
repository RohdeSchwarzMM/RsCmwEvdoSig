State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:SIGNaling<Instance>:CSWitched:STATe

.. code-block:: python

	FETCh:EVDO:SIGNaling<Instance>:CSWitched:STATe



.. autoclass:: RsCmwEvdoSig.Implementations.Cswitched_.State.State
	:members:
	:undoc-members:
	:noindex: