Pdata
----------------------------------------





.. autoclass:: RsCmwEvdoSig.Implementations.Pdata.Pdata
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.pdata.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Pdata_State.rst