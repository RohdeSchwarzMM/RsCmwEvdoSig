State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:SIGNaling<Instance>:PDATa:STATe

.. code-block:: python

	FETCh:EVDO:SIGNaling<Instance>:PDATa:STATe



.. autoclass:: RsCmwEvdoSig.Implementations.Pdata_.State.State
	:members:
	:undoc-members:
	:noindex: