Per
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:EVDO:SIGNaling<Instance>:PER
	single: STOP:EVDO:SIGNaling<Instance>:PER
	single: ABORt:EVDO:SIGNaling<Instance>:PER

.. code-block:: python

	INITiate:EVDO:SIGNaling<Instance>:PER
	STOP:EVDO:SIGNaling<Instance>:PER
	ABORt:EVDO:SIGNaling<Instance>:PER



.. autoclass:: RsCmwEvdoSig.Implementations.Per.Per
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Per_State.rst