State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:SIGNaling<Instance>:PER:STATe

.. code-block:: python

	FETCh:EVDO:SIGNaling<Instance>:PER:STATe



.. autoclass:: RsCmwEvdoSig.Implementations.Per_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.per.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Per_State_All.rst