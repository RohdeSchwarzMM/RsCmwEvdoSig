Route
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:EVDO:SIGNaling<Instance>

.. code-block:: python

	ROUTe:EVDO:SIGNaling<Instance>



.. autoclass:: RsCmwEvdoSig.Implementations.Route.Route
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario.rst