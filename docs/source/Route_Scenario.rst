Scenario
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:EVDO:SIGNaling<Instance>:SCENario:SCELl
	single: ROUTe:EVDO:SIGNaling<Instance>:SCENario:HMODe
	single: ROUTe:EVDO:SIGNaling<Instance>:SCENario:HMLite
	single: ROUTe:EVDO:SIGNaling<Instance>:SCENario

.. code-block:: python

	ROUTe:EVDO:SIGNaling<Instance>:SCENario:SCELl
	ROUTe:EVDO:SIGNaling<Instance>:SCENario:HMODe
	ROUTe:EVDO:SIGNaling<Instance>:SCENario:HMLite
	ROUTe:EVDO:SIGNaling<Instance>:SCENario



.. autoclass:: RsCmwEvdoSig.Implementations.Route_.Scenario.Scenario
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_ScFading.rst
	Route_Scenario_HmFading.rst