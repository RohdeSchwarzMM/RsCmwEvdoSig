ScFading
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:EVDO:SIGNaling<Instance>:SCENario:SCFading:EXTernal
	single: ROUTe:EVDO:SIGNaling<Instance>:SCENario:SCFading:INTernal

.. code-block:: python

	ROUTe:EVDO:SIGNaling<Instance>:SCENario:SCFading:EXTernal
	ROUTe:EVDO:SIGNaling<Instance>:SCENario:SCFading:INTernal



.. autoclass:: RsCmwEvdoSig.Implementations.Route_.Scenario_.ScFading.ScFading
	:members:
	:undoc-members:
	:noindex: