FlPer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:SIGNaling<Instance>:RXQuality:FLPer
	single: FETCh:EVDO:SIGNaling<Instance>:RXQuality:FLPer
	single: CALCulate:EVDO:SIGNaling<Instance>:RXQuality:FLPer

.. code-block:: python

	READ:EVDO:SIGNaling<Instance>:RXQuality:FLPer
	FETCh:EVDO:SIGNaling<Instance>:RXQuality:FLPer
	CALCulate:EVDO:SIGNaling<Instance>:RXQuality:FLPer



.. autoclass:: RsCmwEvdoSig.Implementations.RxQuality_.FlPer.FlPer
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.flPer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_FlPer_State.rst
	RxQuality_FlPer_Cstate.rst