State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:SIGNaling<Instance>:RXQuality:FLPer:STATe

.. code-block:: python

	FETCh:EVDO:SIGNaling<Instance>:RXQuality:FLPer:STATe



.. autoclass:: RsCmwEvdoSig.Implementations.RxQuality_.FlPer_.State.State
	:members:
	:undoc-members:
	:noindex: