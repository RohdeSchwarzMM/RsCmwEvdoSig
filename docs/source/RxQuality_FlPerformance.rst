FlPerformance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance
	single: FETCh:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance
	single: CALCulate:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance

.. code-block:: python

	READ:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance
	FETCh:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance
	CALCulate:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance



.. autoclass:: RsCmwEvdoSig.Implementations.RxQuality_.FlPerformance.FlPerformance
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.flPerformance.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_FlPerformance_State.rst
	RxQuality_FlPerformance_Cstate.rst