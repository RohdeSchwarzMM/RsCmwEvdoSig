State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance:STATe

.. code-block:: python

	FETCh:EVDO:SIGNaling<Instance>:RXQuality:FLPFormance:STATe



.. autoclass:: RsCmwEvdoSig.Implementations.RxQuality_.FlPerformance_.State.State
	:members:
	:undoc-members:
	:noindex: