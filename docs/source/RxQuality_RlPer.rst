RlPer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:SIGNaling<Instance>:RXQuality:RLPer
	single: FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPer
	single: CALCulate:EVDO:SIGNaling<Instance>:RXQuality:RLPer

.. code-block:: python

	READ:EVDO:SIGNaling<Instance>:RXQuality:RLPer
	FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPer
	CALCulate:EVDO:SIGNaling<Instance>:RXQuality:RLPer



.. autoclass:: RsCmwEvdoSig.Implementations.RxQuality_.RlPer.RlPer
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.rlPer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_RlPer_State.rst
	RxQuality_RlPer_Cstate.rst