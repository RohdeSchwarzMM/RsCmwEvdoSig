Cstate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPer:CSTate

.. code-block:: python

	FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPer:CSTate



.. autoclass:: RsCmwEvdoSig.Implementations.RxQuality_.RlPer_.Cstate.Cstate
	:members:
	:undoc-members:
	:noindex: