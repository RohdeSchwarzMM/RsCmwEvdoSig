State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPer:STATe

.. code-block:: python

	FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPer:STATe



.. autoclass:: RsCmwEvdoSig.Implementations.RxQuality_.RlPer_.State.State
	:members:
	:undoc-members:
	:noindex: