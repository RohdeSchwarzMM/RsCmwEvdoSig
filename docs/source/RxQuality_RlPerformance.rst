RlPerformance
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance
	single: FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance
	single: CALCulate:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance

.. code-block:: python

	READ:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance
	FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance
	CALCulate:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance



.. autoclass:: RsCmwEvdoSig.Implementations.RxQuality_.RlPerformance.RlPerformance
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.rlPerformance.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_RlPerformance_State.rst
	RxQuality_RlPerformance_Cstate.rst