State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance:STATe

.. code-block:: python

	FETCh:EVDO:SIGNaling<Instance>:RXQuality:RLPFormance:STATe



.. autoclass:: RsCmwEvdoSig.Implementations.RxQuality_.RlPerformance_.State.State
	:members:
	:undoc-members:
	:noindex: