Sense
----------------------------------------





.. autoclass:: RsCmwEvdoSig.Implementations.Sense.Sense
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Test.rst
	Sense_IqOut.rst
	Sense_AnAddress.rst
	Sense_AtAddress.rst
	Sense_RxQuality.rst
	Sense_Elog.rst