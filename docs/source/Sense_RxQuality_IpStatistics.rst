IpStatistics
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:STATe
	single: SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:RESet
	single: SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:RACK
	single: SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:NAK
	single: SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:SUMMary
	single: SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:PPPTotal
	single: SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:DRATe

.. code-block:: python

	SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:STATe
	SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:RESet
	SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:RACK
	SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:NAK
	SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:SUMMary
	SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:PPPTotal
	SENSe:EVDO:SIGNaling<Instance>:RXQuality:IPSTatistics:DRATe



.. autoclass:: RsCmwEvdoSig.Implementations.Sense_.RxQuality_.IpStatistics.IpStatistics
	:members:
	:undoc-members:
	:noindex: