RfSettings
----------------------------------------





.. autoclass:: RsCmwEvdoSig.Implementations.Source_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_RfSettings_Tx.rst
	Source_RfSettings_Rx.rst