Tx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SOURce:EVDO:SIGNaling<Instance>:RFSettings:TX:EATTenuation

.. code-block:: python

	SOURce:EVDO:SIGNaling<Instance>:RFSettings:TX:EATTenuation



.. autoclass:: RsCmwEvdoSig.Implementations.Source_.RfSettings_.Tx.Tx
	:members:
	:undoc-members:
	:noindex: