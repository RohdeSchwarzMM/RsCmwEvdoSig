Throughput
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:EVDO:SIGNaling<Instance>:THRoughput
	single: STOP:EVDO:SIGNaling<Instance>:THRoughput
	single: ABORt:EVDO:SIGNaling<Instance>:THRoughput

.. code-block:: python

	INITiate:EVDO:SIGNaling<Instance>:THRoughput
	STOP:EVDO:SIGNaling<Instance>:THRoughput
	ABORt:EVDO:SIGNaling<Instance>:THRoughput



.. autoclass:: RsCmwEvdoSig.Implementations.Throughput.Throughput
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_State.rst