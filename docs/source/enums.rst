Enums
=========

AccessDuration
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AccessDuration.S128
	# All values (4x):
	S128 | S16 | S32 | S64

ApplicationMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ApplicationMode.FAR
	# All values (4x):
	FAR | FWD | PACKet | REV

ApplyTimeAt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ApplyTimeAt.EVER
	# All values (3x):
	EVER | NEXT | SUSO

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

AwgnMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AwgnMode.HPOWer
	# All values (2x):
	HPOWer | NORMal

BandClass
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BandClass.AWS
	# Last value:
	value = enums.BandClass.USPC
	# All values (23x):
	AWS | B18M | IEXT | IM2K | JTAC | KCEL | KPCS | LBANd
	LO7C | N45T | NA7C | NA8S | NA9C | NAPC | PA4M | PA8M
	PS7C | SBANd | TACS | U25B | U25F | USC | USPC

CarrierStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CarrierStatus.INACtive
	# All values (4x):
	INACtive | OK | STALe | VIOLated

ConnectionState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnectionState.CONNected
	# All values (7x):
	CONNected | IDLE | OFF | ON | PAGing | SNEGotiation | SOPen

CswitchedAction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CswitchedAction.CLOSe
	# All values (4x):
	CLOSe | CONNect | DISConnect | HANDoff

CtrlChannelDataRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CtrlChannelDataRate.R384
	# All values (2x):
	R384 | R768

DisplayTab
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DisplayTab.CTRLchper
	# All values (6x):
	CTRLchper | DATA | OVERview | PER | RLQ | THRoughput

ExpPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ExpPowerMode.AUTO
	# All values (5x):
	AUTO | MANual | MAX | MIN | OLRule

Fmode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Fmode.AALWays
	# All values (3x):
	AALWays | NAALways | NUSed

FsimStandard
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FsimStandard.P1
	# All values (5x):
	P1 | P2 | P3 | P4 | P5

InsertLossMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InsertLossMode.NORMal
	# All values (2x):
	NORMal | USER

IpAddressIndex
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpAddressIndex.IP1
	# All values (3x):
	IP1 | IP2 | IP3

KeepConstant
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.KeepConstant.DSHift
	# All values (2x):
	DSHift | SPEed

LinkCarrier
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LinkCarrier.ACTive
	# All values (4x):
	ACTive | DISabled | NACTive | NCConnected

LogCategory
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogCategory.CONTinue
	# All values (4x):
	CONTinue | ERRor | INFO | WARNing

LteBand
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.LteBand.OB1
	# Last value:
	value = enums.LteBand.UDEFined
	# All values (45x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB15 | OB16
	OB17 | OB18 | OB19 | OB2 | OB20 | OB21 | OB22 | OB23
	OB24 | OB25 | OB26 | OB27 | OB28 | OB29 | OB3 | OB30
	OB31 | OB32 | OB33 | OB34 | OB35 | OB36 | OB37 | OB38
	OB39 | OB4 | OB40 | OB41 | OB42 | OB43 | OB44 | OB5
	OB6 | OB7 | OB8 | OB9 | UDEFined

MainGenState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MainGenState.OFF
	# All values (3x):
	OFF | ON | RFHandover

NetworkRelease
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetworkRelease.R0
	# All values (3x):
	R0 | RA | RB

NetworkSegment
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetworkSegment.A
	# All values (3x):
	A | B | C

PacketSize
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PacketSize.S128
	# Last value:
	value = enums.PacketSize.TOTal
	# All values (12x):
	S128 | S1K | S256 | S2K | S3K | S4K | S512 | S5K
	S6K | S7K | S8K | TOTal

PdState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PdState.CONNected
	# All values (4x):
	CONNected | DORMant | OFF | ON

PerEvaluation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PerEvaluation.ALLCarriers
	# All values (2x):
	ALLCarriers | PERCarrier

PerStopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PerStopCondition.ALEXceeded
	# All values (4x):
	ALEXceeded | MCLexceeded | MPERexceeded | NONE

PlSlots
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PlSlots.S16
	# All values (2x):
	S16 | S4

PlSubtype
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PlSubtype.ST01
	# All values (3x):
	ST01 | ST2 | ST3

PowerCtrlBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerCtrlBits.ADOWn
	# All values (6x):
	ADOWn | AUP | AUTO | HOLD | PATTern | RTESt

PrefApplication
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PrefApplication.DPA
	# All values (2x):
	DPA | EMPA

PrefAppMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PrefAppMode.EHRPd
	# All values (2x):
	EHRPd | HRPD

ProbesAckMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ProbesAckMode.ACKN
	# All values (2x):
	ACKN | IGN

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

RevLinkPerDataRate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RevLinkPerDataRate.R0K0
	# Last value:
	value = enums.RevLinkPerDataRate.TOTal
	# All values (14x):
	R0K0 | R115k2 | R1228k8 | R153k6 | R1843k2 | R19K2 | R230k4 | R307k2
	R38K4 | R460k8 | R614k4 | R76K8 | R921k6 | TOTal

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (154x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IF1 | IF2 | IF3 | IQ1I | IQ3I | IQ5I | IQ7I | R11
	R11C | R12 | R12C | R12I | R13 | R13C | R14 | R14C
	R14I | R15 | R16 | R17 | R18 | R21 | R21C | R22
	R22C | R22I | R23 | R23C | R24 | R24C | R24I | R25
	R26 | R27 | R28 | R31 | R31C | R32 | R32C | R32I
	R33 | R33C | R34 | R34C | R34I | R35 | R36 | R37
	R38 | R41 | R41C | R42 | R42C | R42I | R43 | R43C
	R44 | R44C | R44I | R45 | R46 | R47 | R48 | RA1
	RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8 | RB1
	RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8 | RC1
	RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8 | RD1
	RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF1C | RF2 | RF2C | RF2I | RF3 | RF3C | RF4 | RF4C
	RF4I | RF5 | RF5C | RF6 | RF6C | RF7 | RF8 | RFAC
	RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

RxSignalState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RxSignalState.HIGH
	# All values (4x):
	HIGH | LOW | NAV | OK

SampleRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SampleRate.M1
	# All values (8x):
	M1 | M100 | M15 | M19 | M3 | M30 | M7 | M9

SamRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SamRate.R19K
	# All values (3x):
	R19K | R38K | R9K

Scenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Scenario.HMFading
	# All values (6x):
	HMFading | HMLite | HMODe | SCELl | SCFading | UNDefined

SectorIdFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SectorIdFormat.A41N
	# All values (2x):
	A41N | MANual

SegmentBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SegmentBits.ALTernating
	# All values (3x):
	ALTernating | DOWN | UP

SlopeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlopeType.NEGative
	# All values (2x):
	NEGative | POSitive

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

SyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncState.ADINtermed
	# All values (7x):
	ADINtermed | ADJusted | INValid | OFF | ON | PENDing | RFHandover

T2Pmode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.T2Pmode.RFCO
	# All values (2x):
	RFCO | TPUT

TimeSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeSource.CMWTime
	# All values (3x):
	CMWTime | DATE | SYNC

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (77x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IF1 | IF2 | IF3 | IQ2O | IQ4O | IQ6O | IQ8O | R118
	R1183 | R1184 | R11C | R11O | R11O3 | R11O4 | R12C | R13C
	R13O | R14C | R214 | R218 | R21C | R21O | R22C | R23C
	R23O | R24C | R258 | R318 | R31C | R31O | R32C | R33C
	R33O | R34C | R418 | R41C | R41O | R42C | R43C | R43O
	R44C | RA18 | RB14 | RB18 | RC18 | RD18 | RE18 | RF18
	RF1C | RF1O | RF2C | RF3C | RF3O | RF4C | RF5C | RF6C
	RFAC | RFAO | RFBC | RG18 | RH18

TxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConverter.ITX1
	# Last value:
	value = enums.TxConverter.TX44
	# All values (40x):
	ITX1 | ITX11 | ITX12 | ITX13 | ITX14 | ITX2 | ITX21 | ITX22
	ITX23 | ITX24 | ITX3 | ITX31 | ITX32 | ITX33 | ITX34 | ITX4
	ITX41 | ITX42 | ITX43 | ITX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

