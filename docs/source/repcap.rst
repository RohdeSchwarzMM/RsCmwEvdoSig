RepCaps
=========



Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst16
	# All values (16x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16

CellNo
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.CellNo.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

IpAddress
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IpAddress.Version4
	# Values (2x):
	Version4 | Version6

NeighborCell
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.NeighborCell.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

Path
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Path.Nr1
	# Values (2x):
	Nr1 | Nr2

Segment
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Segment.S1
	# Values (4x):
	S1 | S2 | S3 | S4

